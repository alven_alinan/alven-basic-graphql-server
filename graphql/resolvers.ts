import { mergeResolvers } from 'merge-graphql-schemas';
import * as path from 'path';

import { contentResolver } from './resolvers/content-resolver';
import { userResolver } from './resolvers/user-resolver';
import { playlistResolver } from './resolvers/playlist-resolver';

const resolversArray = [
    contentResolver,
    userResolver,
    playlistResolver,
];

export const resolvers = mergeResolvers(resolversArray);
