import { dataSource } from '../../datasources';

export const userResolver = {
  Query: {
    users() {
      return dataSource.contentService.getUsers()
    },

    user(parent, args) {
      return dataSource.contentService.getUser(args.id)
    }
  },

  User: {
    playlists(parent) {
      return dataSource.contentService.getUserPlaylists(parent.id)
    }
  }
};
