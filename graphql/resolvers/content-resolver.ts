import { dataSource } from '../../datasources';

export const contentResolver = {
  Query: {
    contents: () => {
      return dataSource.contentService.getContents()
    },

    content(parent, args) {
      return dataSource.contentService.getContent(args.id)
    }
  },
  
  Mutation: {
    addContent(parent, args) {
      return dataSource.contentService.addContent({
        title: args.title,
        year: args.year,
      });
    }
  }
};
