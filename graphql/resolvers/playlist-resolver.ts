import { dataSource } from '../../datasources';
import { argumentsObjectFromField } from 'apollo-utilities';

export const playlistResolver = {
  Query: {
    playlists() {
      return dataSource.contentService.getPlaylists();
    },
    playlist(parent, args) {
      return dataSource.contentService.getPlaylistById(args.id);
    },
  },

  Mutation: {
    createPlaylist(parent, args) {
      const playlist = dataSource.contentService.addPlaylist({
        name: args.name,
        userId: args.userId,
      });

      return playlist;
    },
    addItemToPlaylist(parent, args) {
      return dataSource.contentService.addItemInPlaylist({
        contentId: args.contentId,
        playlistId: args.playlistId,
      });
    }
  },

  Playlist: {
    contents(parent) {
      return dataSource.contentService.getContentsInPlaylist(parent.id);
    },
  }
};
