### Instructions

- Use nodejs v11.7.0 (nvm install 11.7.0)
- Run `npm install`
- This sample of GraphQL in server uses `graphql-yoga` library


### Sample Queries, Mutations and Subscriptions
#
```
query getUser($userId: ID) {
  user(id: $userId) {
    name
    playlists {
      id
      name
      contents {
        id
        title
        year
      }
    }
  }
}

query viewPlaylists {
  playlists {
    id
    name
    contents {
      title
    }
  }
}

query viewAllContents {
  contents {
    id
    title
    year
  }
}

mutation addContent($title: String, $year: Int) {
  addContent(title: $title, year: $year) {
    id
    title
    year
  }
}

mutation playlist($userId: ID, $watchlistName: String) {
  createPlaylist(name: $watchlistName, userId: $userId) {
    name
    contents {
      title
    }
  }
}

mutation addItemsToPlaylist($playlistId: ID, $contentId: ID) {
  addItemToPlaylist(playlistId: $playlistId, contentId: $contentId)
}
```