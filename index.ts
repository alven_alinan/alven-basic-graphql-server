import { GraphQLServer } from 'graphql-yoga';

import { typeDefs } from './graphql/typedefs';
import { resolvers } from './graphql/resolvers';

const PORT = process.env.PORT || 7000;

const server = new GraphQLServer({
  typeDefs,
  resolvers,
});

server.start({ port: PORT }, () => console.info(`Running on localhost:${PORT}`));
