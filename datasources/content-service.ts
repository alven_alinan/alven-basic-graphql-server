import _ from 'lodash';
import hyphenate from 'hyphenate';

const findVideo = (contentId) => contents.find(({ id }) => contentId === id);
const generateNewId = (arr) => arr.sort((first, next) => next.id - first.id)[0]['id'] + 1;

const contents = [
    {
      id: 1,
      title: 'Captain America: The First Avenger',
      year: 2011,
    },
    {
      id: 2,
      title: '300: Rise Of An Empire',
      year: 2014,
    },
    {
      id: 3,
      title: 'Toy Story',
      year: 1995,
    },
    {
      id: 4,
      title: 'The Lion King',
      year: 1994,
    },
    {
       id: 5,
       title: 'Iron Man 3',
       year: 2013,
    }
  ];
  
const playlists = [
    {
      id: 'playlist-1',
      name: 'Watch Later 1',
      userId: 'alven-a',
    },
    {
      id: 'playlist-2',
      name: 'Watch Later 2',
      userId: 'alven-a',
    },
    {
      id: 'playlist-3',
      name: 'Ninja',
      userId: 'francis-c'
    },
];
  
const contentPlaylists = [
    {
      id: 1,
      playlistId: 'playlist-1',
      contentId: 1,
    },
    {
      id: 2,
      playlistId: 'playlist-1',
      contentId: 3,
    },
    {
      id: 3,
      playlistId: 'playlist-2',
      contentId: 1,
    },
    {
      id: 4,
      playlistId: 'playlist-3',
      contentId: 2,
    },
    {
      id: 5,
      playlistId: 'playlist-3',
      contentId: 4,
    },
    {
      id: 6,
      playlistId: 'playlist-1',
      contentId: 5,
    },
];

const users = [
    {
      id: 'alven-a',
      name: 'Alven',
      birthdate: '1988-10-17',
    },
    {
      id: 'francis-c',
      name: 'Francis',
      birthdate: '1988-08-27',
    }
];

export class ContentService {
  constructor() {
  }

  // get single user
  getUser(userId) {
    return users.filter(({ id }) => id === userId)[0];
  }

  // get single video
  getContent(contentId) {
    return contents.filter(({ id }) => id === parseInt(contentId));
  }

  getUsers() {
    return users;
  }

  getContents() {
    return contents;
  }

  getPlaylists() {
    return playlists;
  }

  getcontentPlaylists() {
    return contentPlaylists;
  }

  getPlaylistById(playlistId) {
    return playlists.filter(({ id }) => id === playlistId)[0];
  }

  getUserPlaylists(id) {
    return playlists.filter(({ userId }) => userId === id);
  }

  getContentsInPlaylist(id) {
    return contentPlaylists
              .filter(({ playlistId }) => id === playlistId)
              .map(({ contentId }) => findVideo(contentId));
  }

  addPlaylist(playlist) {
    const id = hyphenate(playlist.name, { lowerCase: true });
    playlists.push({
      id,
      name: playlist.name,
      userId: playlist.userId,
    });

    return playlist;
  }

  addItemInPlaylist({ playlistId, contentId }) {
    const id = generateNewId(contentPlaylists);

    contentPlaylists.push({
      playlistId: playlistId,
      contentId: parseInt(contentId, 10),
      id,
    });

    console.info(contentPlaylists);

    return 'Done!';
  }

  addContent({ title, year }) {
    const id = generateNewId(contents);
    const content = {
      id,
      title,
      year,
    }

    contents.push(content);

    return content;
  }

  deletePlaylist(id) {
    const contentPlaylist = contentPlaylists.filter(({ playlistId }) => id !== playlistId);
    const playlsts = playlists.filter(({ id: playlistId }) => id !== playlistId);
  }
}
